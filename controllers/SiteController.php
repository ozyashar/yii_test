<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	//////////////////////////////////////////////////// RBAC  /////////////////////////////////////////
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$manager = $auth->createRole('manager');
		$auth->add($manager);
		
		$member = $auth->createRole('member');
		$auth->add($member);
		
		$teamleader = $auth->createRole('teamleader');
		$auth->add($teamleader);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionManagerpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$viewBreakdownstatus = $auth->createPermission('viewBreakdownstatus');
		$viewBreakdownstatus->description = 'manager can view only breakdown with status urgent';
		$auth->add($viewBreakdownstatus);
	}


	public function actionMemberpermissions()
	{
		$auth = Yii::$app->authManager;

		$createBreakdown = $auth->createPermission('createBreakdown'); 
		$createBreakdown->description = 'member can create breakdown';
		$auth->add($createBreakdown);	

		$updateBreakdown = $auth->createPermission('updateBreakdown'); 
		$updateBreakdown->description = 'member can update breakdown';
		$auth->add($updateBreakdown);	

		$indexBreakdown = $auth->createPermission('indexBreakdown'); 
		$indexBreakdown->description = 'member can index breakdown';
		$auth->add($indexBreakdown);
		
		$viewBreakdown = $auth->createPermission('viewBreakdown'); 
		$viewBreakdown->description = 'member can view breakdown';
		$auth->add($viewBreakdown);
		
		$viewOwnUser = $auth->createPermission('viewOwnUser'); 
		$viewOwnUser->description = 'member can view only his user details';
		$auth->add($viewOwnUser);
	
	}
	

	public function actionTeamleaderpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateLevel = $auth->createPermission('updateLevel'); 
		$updateLevel->description = 'teamleader can update level ';
		$auth->add($updateLevel);

		$updateStatus = $auth->createPermission('updateStatus'); 
		$updateStatus->description = 'teamleader can update status';
		$auth->add($updateStatus);		
	}

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUsers = $auth->createPermission('createUsers'); 
		$createUsers->description = 'admin can create new users ';
		$auth->add($createUsers);

		$updateUsers = $auth->createPermission('updateUsers'); 
		$updateUsers->description = 'admin can update users';
		$auth->add($updateUsers);

		$deleteUsers = $auth->createPermission('deleteUsers');  
		$deleteUsers->description = 'admin can delete users';
		$auth->add($deleteUsers);
		
		
	}
	
	//Childs
	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$manager = $auth->getRole('manager'); 

		$viewBreakdownstatus = $auth->getPermission('viewBreakdownstatus'); 
		$auth->addChild($manager, $viewBreakdownstatus);
		
		

		////////////////////////////////////
		
		$member = $auth->getRole('member'); 
		$auth->addChild($member, $manager);
		
		$createBreakdown = $auth->getPermission('createBreakdown'); 
		$auth->addChild($member, $createBreakdown);

		$updateBreakdown = $auth->getPermission('updateBreakdown'); 
		$auth->addChild($member, $updateBreakdown);	

		$indexBreakdown = $auth->getPermission('indexBreakdown'); 
		$auth->addChild($member, $indexBreakdown);

		$viewBreakdown = $auth->getPermission('viewBreakdown'); 
		$auth->addChild($member, $viewBreakdown);	

		$viewOwnUser = $auth->getPermission('viewOwnUser'); 
		$auth->addChild($member, $viewOwnUser);


		///////////////////////////////////////
		
		$teamleader = $auth->getRole('teamleader'); 
		$auth->addChild($teamleader, $member);
		
		$updateLevel = $auth->getPermission('updateLevel'); 
		$auth->addChild($teamleader, $updateLevel);

		$updateStatus = $auth->getPermission('updateStatus'); 
		$auth->addChild($teamleader, $updateStatus);	
	
		
		//////////////////////////////////////////////
		
		$admin = $auth->getRole('admin'); 
		$auth->addChild($admin, $teamleader);	
		
		$createUsers = $auth->getPermission('createUsers'); 
		$auth->addChild($admin, $createUsers);
		
		$updateUsers = $auth->getPermission('updateUsers'); 
		$auth->addChild($admin, $updateUsers);
		
		$deleteUsers = $auth->getPermission('deleteUsers'); 
		$auth->addChild($admin, $deleteUsers);
	
		

	}
}
