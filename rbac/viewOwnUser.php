<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class viewOwnUser extends Rule
{
	public $name = 'viewOwnUser';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['breakdown']) ? $params['breakdown']->member == $user : false;
		}
		return false;
	}
}