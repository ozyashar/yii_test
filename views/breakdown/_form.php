<?php

use app\models\Level;
use app\models\Status;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	
	<?php if (\Yii::$app->user->can('updateLevel', ['user' =>$model]) )  {?>
    <?= $form->field($model, 'level')->
									dropDownList(Level::getLevels())?>
	<?php } //  level: only teamleader can change the level, the other one can't see it?>


	<?php if (\Yii::$app->user->can('updateStatus', ['user' =>$model]) )  {?>
    <?= $form->field($model, 'status')->
									dropDownList(Status::getStatuses())?>
	<?php } //  status: only teamleader can change the status, the other one can't see it?>
	
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
