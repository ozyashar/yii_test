<?php

use yii\db\Migration;

class m170806_053611_breakdown_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'breakdown',
            [
				'id' => 'pk',
                'title' => 'string',	
				'level' => 'integer',
				'status' => 'integer'
				
				],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
        $this->dropTable('breakdown');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
