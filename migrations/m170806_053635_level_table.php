<?php

use yii\db\Migration;

class m170806_053635_level_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'level',
            [
				'id' => 'pk',
                'level_name' => 'string'
				
				],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('level');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
