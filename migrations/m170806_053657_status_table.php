<?php

use yii\db\Migration;

class m170806_053657_status_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'status',
            [
				'id' => 'pk',
                'status_name' => 'string'
				
				],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
       $this->dropTable('status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
